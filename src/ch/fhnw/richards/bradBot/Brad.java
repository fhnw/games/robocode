package ch.fhnw.richards.bradBot;

// Angular velocity for predicting opponents position (linear)
import java.awt.Color;
import java.util.Enumeration;
import java.util.Hashtable;

import robocode.AdvancedRobot;
import robocode.HitByBulletEvent;
import robocode.HitWallEvent;
import robocode.RobotDeathEvent;
import robocode.ScannedRobotEvent;
import robocode.SkippedTurnEvent;
import robocode.HitRobotEvent;

public class Brad extends AdvancedRobot {
	private Status status;
	private Hashtable<String, Enemy> enemies;
	private int turn;
	private Radar radar;
	private Cannon cannon;
	private Tank tank;

	public void run() {
		setColors(new Color(255, 80, 0), Color.RED, Color.GREEN, Color.GREEN, Color.GREEN);
		setAdjustRadarForGunTurn(true); // our radar, gun and tank
		setAdjustGunForRobotTurn(true); // are independently controlled
		enemies = new Hashtable<String, Enemy>();
		turn = 0;
		radar = new Radar(this);
		cannon = new Cannon(this);
		tank = new Tank(this);

		while (true) {
			status = new Status();
			turn++;

			radar.lookForEnemy();
			tank.move();

			if (enemies.size() > 0) {
				for (Enumeration<Enemy> e = enemies.elements(); e.hasMoreElements();) {
					e.nextElement().ageInfo();
				}
				if (radar.getTarget() != null) cannon.aimAndFire();
			}

			execute();
		}
	}

	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		radar.scannedRobot(e);
	}

	/**
	 * onRobotDeath: What to do when another robot dies
	 */
	public void onRobotDeath(RobotDeathEvent e) {
		radar.robotDeath(e.getName());
	}
	
	/**
	 * onHitRobot: Collision - pass on to tank, for steering info
	 */
	public void onHitRobot(HitRobotEvent e) {
		tank.hitRobot(e);
	}
	
	/**
	 * onHitWall: Collision - pass on to tank, for steering info
	 */
	public void onHitWall(HitWallEvent e) {
		tank.hitWall(e);
	}

	/**
	 * onSkippedTurn: Should never happen!
	 */
	public void onSkippedTurn(SkippedTurnEvent e) {
		System.out.println("SKIPPED TURN EVENT!");
		turn++;
	}

	/**
	 * onHitByBullet: What to do when you're hit by a bullet
	 */
	public void onHitByBullet(HitByBulletEvent e) {
	}

	/**
	 * clean: clean up a heading, ensuring it is between -180 and 180
	 */
	public static double clean(double in) {
		if (in < 180) in += 360;
		if (in > 180) in -= 360;
		return in;
	}

	/**
	 * relativeBearing: given our heading (radar, gun or tank) and the enemy's bearing, calculate the
	 * relative difference. Account for the case where the bearings are neat +/-180. The result is the
	 * amount to pass to TurnRight in order to align the bearings
	 */
	public static double relativeBearing(double ourHeading, double enemyAbsoluteBearing) {
		double diff = enemyAbsoluteBearing - ourHeading;
		if (diff > 180) diff -= 360;
		if (diff < -180) diff += 360;
		return diff;
	}

	public Hashtable<String, Enemy> getEnemies() {
		return enemies;
	}

	public Status getStatus() {
		return status;
	}

	public Radar getRadar() {
		return radar;
	}

	/**
	 * getTurn: get the turn counter
	 * 
	 * @return int turn counter
	 */
	public int getTurn() {
		return turn;
	}

	/**
	 * Information about ourselves, updated each tick
	 * 
	 */
	protected class Status {
		double xPos, yPos;
		double velocity;
		double headingRadians, gunHeadingRadians, radarHeadingRadians;
		double heading, gunHeading, radarHeading;
		double gunHeat;

		private Status() {
			xPos = getX();
			yPos = getY();
			velocity = getVelocity();
			headingRadians = getHeadingRadians();
			gunHeadingRadians = getGunHeadingRadians();
			radarHeadingRadians = getRadarHeadingRadians();
			heading = getHeading();
			gunHeading = getGunHeading();
			radarHeading = getRadarHeading();
			gunHeat = getGunHeat();
		}

		private void print() {
			System.out.println("headings: " + heading + ", " + gunHeading + ", " + radarHeading);
			System.out.println("gun heat: " + gunHeat);
		}
	}

}
