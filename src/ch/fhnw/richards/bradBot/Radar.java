package ch.fhnw.richards.bradBot;

// If enemy not immediately found by locateEnemy, then keep turning in the same direction

import java.util.Enumeration;
import java.util.Hashtable;

import robocode.AdvancedRobot;
import robocode.ScannedRobotEvent;
import robocode.util.Utils;

/**
 * The radar operated in three modes, and automatically changes amongst them: Survey mode (0):
 * rotate the radar through a full circle, to find all enemies Target locate (1): locate the
 * selected target with the radar Target track (2): hold the radar focused on the target.
 * 
 * The radar remains in survey mode as long as the gun-heat is greater than a given threshold; it
 * also enters survey-mode if the current target is lost from the radar for any reason (including
 * being destroyed). Once in survey mode, the radar will complete at least one full circle.
 * 
 * In target locate mode, a target has been chosen from the list of enemies. The radar scans until
 * this target has been found. The radar attempts to scan in the direction that will find the target
 * most quickly.
 * 
 * In target hold mode, the radar sweeps back and forth across the target, to ensure that the target
 * remains in view and that we have current information. Presumably the gun will eventually fire,
 * raising the gun heat and putting us back into survey mode.
 * 
 * @author Administrator
 * 
 */
public class Radar {
	// if information is this old, it is no longer useful
	private final int TRACKING_LOST = 16;

	private Brad robot;
	private Hashtable<String, Enemy> enemies;
	private Enemy enemyToTrack;
	private TrackingInfo enemyTrackingInfo;
	private int mode; // eventually enumerated. 0 = survey

	// Mode constants (eventually should be enumerated)
	private final int SURVEY_MODE = 0;
	private final int LOCATE_MODE = 1;
	private final int TRACK_MODE = 2;
	private final double SURVEY_THRESHOLD_GUN_HEAT = 1.0;
	private int surveySteps = 0;
	private int turnsTracking = 0;

	protected Radar(Brad robot) {
		this.robot = robot;
		this.enemies = robot.getEnemies();
	}

	public void scannedRobot(ScannedRobotEvent scanEvent) {
		String scannedName = scanEvent.getName();
		if (enemies.containsKey(scannedName)) {
			Enemy enemy = enemies.get(scannedName);
			enemy.setScanEvent(scanEvent, robot);
		} else {
			Enemy enemy = new Enemy(scanEvent, robot);
			enemies.put(scannedName, enemy);
		}

		if ((enemyToTrack != null) && (mode == LOCATE_MODE) && (scannedName.equals(enemyToTrack.getName()))) {
			// found the target
			mode = TRACK_MODE;
		}
	}

	/**
	 * robotDeath: called when another robot dies. If it was our target, we set our target back to
	 * null. We also remove the robot from our list of enemies.
	 */
	public void robotDeath(String robotName) {
		// Did our target disappear?
		if (enemyToTrack != null) {
			if (enemyToTrack.getName().equals(robotName)) enemyToTrack = null;
		}

		// remove from list
		enemies.remove(robotName);
	}

	/**
	 * lookForEnemy: the main routine to manage the radar. We choose between two main actions: survey
	 * and track. In survey mode, we rotate the radar through a full circle, to find all enemies. In
	 * track mode, we focus on a single enemy and keep our data continually updated.
	 */
	public void lookForEnemy() {
		// if we are not currently surveying, we may want to start a survey
		if (mode != SURVEY_MODE) {
			if ((robot.getStatus().gunHeat > SURVEY_THRESHOLD_GUN_HEAT) || (enemies.size() == 0)
					|| (enemyToTrack == null) || turnsTracking > 50) {
				turnsTracking = 0;
				mode = SURVEY_MODE;
				surveySteps = 0; // reset number of survey steps taken so far...
				enemyToTrack = null; // reset the enemy we are tracking
			}
		}

		if (mode == SURVEY_MODE) surveyEnemies();
		if (mode == LOCATE_MODE) locateEnemy();
		if (mode == TRACK_MODE) trackEnemy();
	}
	
	/**
	 * surveyEnemies: swing the radar through a full circle, cataloging all enemies that we find.
	 */

	private void surveyEnemies() {
		robot.setTurnRadarRight(45);
		// A full circle requires 8 steps - if this was step 8, we are done.
		// However, we keep going anyway, as long as the gun is over the threshold
		if (surveySteps >= 7 && robot.getStatus().gunHeat < SURVEY_THRESHOLD_GUN_HEAT) {
			surveySteps = 0;
			mode = LOCATE_MODE;
		} else {
			surveySteps++;
		}
	}

	/**
	 * chooseEnemyToTrack:
	 * 
	 * pick one of our enemies to target (in this case, the closest). Then rotate the radar until we
	 * find them.
	 * 
	 * We remain in this mode until the enemy has been sighted by the radar - possibly over multiple
	 * turns. If the enemy is destroyed while we are trying to find them, we start all over again...
	 * 
	 */
	private void locateEnemy() {
		if (enemyToTrack != null) {
			// ensure the enemy is still alive, and has been seen...
			if (!enemies.contains(enemyToTrack)
					|| (enemyToTrack.getInfoAge() >= TRACKING_LOST)) enemyToTrack = null;
		}

		if (enemyToTrack == null) {
			double closestDistance = 9999;
			for (Enumeration<Enemy> e = enemies.elements(); e.hasMoreElements();) {
				Enemy enemy = e.nextElement();
				double thisDistance = enemy.getScanEvent().getDistance();
				// only bother with enemies we have seen lately...
				if ((enemy.getInfoAge() < TRACKING_LOST) && (thisDistance < closestDistance)) {
					closestDistance = thisDistance;
					enemyToTrack = enemy;
				}
			}
		}

		if (enemyToTrack == null) {
			// shouldn't happen, but if it does, do a new survey
			lookForEnemy();
		} else {
			double enemyBearing = Utils.normalRelativeAngle(enemyToTrack.getAbsoluteBearingRadians() - robot.getStatus().radarHeadingRadians);
			double radarTurn;
			if (enemyBearing < 0) {
				radarTurn = -Math.PI/4;
			} else {
				radarTurn = Math.PI/4;
			}
			robot.setTurnRadarRightRadians(radarTurn);
		}
	}

	/**
	 * trackEnemy: track the given enemy. If we lose them, then set enemyToTrack to null. Obviously,
	 * we can only track an enemy if we have one...
	 */
	private void trackEnemy() {
		if (enemyToTrack != null && enemyToTrack.getInfoAge() < 3) {
			double enemyBearing = Utils.normalRelativeAngle(enemyToTrack.getAbsoluteBearingRadians() - robot.getStatus().radarHeadingRadians);

			double radarTurn;
			if (enemyBearing < 0) {
				radarTurn = enemyBearing - Math.PI/8;
			} else {
				radarTurn = enemyBearing + Math.PI/8;
			}
			robot.setTurnRadarRightRadians(radarTurn);
			turnsTracking++;
		} else {
			// lost track of them
			enemyToTrack = null;
			lookForEnemy();
		}
	}

	public Enemy getTarget() {
		return enemyToTrack;
	}
	
	// Oddly, tracking info for our target is set externally
	public TrackingInfo getTargetTrackingInfo() {
		return enemyTrackingInfo;
	}
	public void setTargetTrackingInfo(TrackingInfo infoIn) {
		enemyTrackingInfo = infoIn;
	}
}
