package ch.fhnw.richards.bradBot;

// Try to maintain a particular distance from enemy...

import java.util.Enumeration;
import java.util.Hashtable;

import robocode.HitRobotEvent;
import robocode.HitWallEvent;
import robocode.ScannedRobotEvent;
import robocode.util.Utils;
import static java.lang.Math.*;

/**
 * General principles of movement: - Stay away from walls - Stay away from other tanks that are in
 * front of us - Approach a distant target
 * 
 * The steering is accomplished by calculating attraction and repulsion - not of the tank, but of a
 * steering point ahead of the tank. Attraction/repulsion forces working left or right on this
 * steering point cause the tank to steer left or right.
 * 
 * The implementation can be fine-tuned through a number of constants affecting the strength of
 * forces, thresholds beyond which we no longer bother calculating them, etc.
 * 
 * To realize this implementation, we require periodic surveys at least of the area directly ahead
 * of us.
 * 
 * @author Brad
 * 
 */
public class Tank {
	// links to information elsewhere
	private Brad robot;
	private Hashtable<String, Enemy> enemies;
	private Radar radar;

	// our own information stores
	private TrackingInfo steeringPoint;
	private TrackingInfo targetCoordinates;
	private Hashtable<String, TrackingInfo> enemyPositions;
	private int blockMovement; // do not move forward for this number of turns
	private double xMax;
	private double yMax;

	// constants used to fine-tune movement behavior
	private final double WALL_THRESHOLD = 100.0; // ignore walls beyond this distance
	private final double TANK_THRESHOLD = 300.0; // ignore tanks beyond this distance
	private final double TARGET_THRESHOLD = 300.0; // move toward target if beyond this distance
	private final double WALL_REPULSION = 20000.0; // wall repulsion factor
	private final double TANK_REPULSION = 30000.0; // tank repulsion factor
	private final double TARGET_ATTRACTION = 0.3; // target attraction factor

	public Tank(Brad robot) {
		this.robot = robot;
		this.radar = robot.getRadar();
		this.enemies = robot.getEnemies();
		xMax = robot.getBattleFieldWidth();
		yMax = robot.getBattleFieldHeight();
		blockMovement = 0;
	}

	/**
	 * Called each turn to move the tank: - We update our steering point to account for our movement
	 * in the last turn. - We ensure that our enemies-list contains an entry for each enemy (and no
	 * entries for dead ones), we age this information, and replace any entries where we have newer
	 * informations from scans. - We calculate the forces on the steering point, to decide how to move
	 * in the coming turn. - We then enter our movement orders for the coming turn.
	 */
	public void move() {
		setSteeringPoint();
		updateEnemies();
		double steeringForce = calculateSteering();
		double targetForce = calculateTargetSteering();
		enterMovementOrders(steeringForce, targetForce);
	}
	
	/**
	 * We have hit another robot. If we rammed him, don't just keep trying to move forward,
	 * as that costs us energy. We block forward movement for 4 turns.
	 */
	public void hitRobot(HitRobotEvent evt) {
		// only pay attention if it was our fault
		if (evt.isMyFault()) {
			blockMovement = 4;
		}
	}

	/**
	 * We have hit a wall. Don't just keep trying to move forward,
	 * as that costs us energy. We block forward movement for 4 turns.
	 */
	public void hitWall(HitWallEvent evt) {
		blockMovement = 4;
	}
	
	private void setSteeringPoint() {
		steeringPoint = new TrackingInfo(robot.getStatus());
	}

	private void updateEnemies() {
		enemyPositions = new Hashtable<String, TrackingInfo>();
		targetCoordinates = null;

		for (Enumeration<Enemy> e = enemies.elements(); e.hasMoreElements();) {
			Enemy enemy = e.nextElement();
			TrackingInfo tmpCoord = new TrackingInfo(enemy);
			enemyPositions.put(enemy.getName(), tmpCoord);
			
			// if we have a target, keep these coordinates around for steering calculations
			Enemy target = robot.getRadar().getTarget();
			if (target == enemy) {
				targetCoordinates = tmpCoord;
				radar.setTargetTrackingInfo(targetCoordinates);
			}
		}
	}

	private double calculateSteering() {
		double xDiff, yDiff;
		double xWallForce = 0, yWallForce = 0;
		// walls
		xDiff = 0;
		if (steeringPoint.xPos < WALL_THRESHOLD) xDiff = steeringPoint.xPos;
		if ((xMax - steeringPoint.xPos) < WALL_THRESHOLD) xDiff = steeringPoint.xPos - xMax;
		if (xDiff != 0) {
			xWallForce = signum(xDiff) * WALL_REPULSION * cos(steeringPoint.velHeading) / (xDiff * xDiff);
		}

		yDiff = 0;
		if (steeringPoint.yPos < WALL_THRESHOLD) yDiff = steeringPoint.yPos;
		if ((yMax - steeringPoint.yPos) < WALL_THRESHOLD) yDiff = steeringPoint.yPos - yMax;
		if (yDiff != 0) {
			yDiff = 0 - yDiff;
			yWallForce = signum(yDiff) * WALL_REPULSION * sin(steeringPoint.velHeading) / (yDiff * yDiff);
		}

		// enemies
		double enemyForce = 0;
		for (Enumeration<TrackingInfo> e = enemyPositions.elements(); e.hasMoreElements();) {
			TrackingInfo enemy = e.nextElement();
			double xDist = enemy.xPos - steeringPoint.xPos;
			double yDist = enemy.yPos - steeringPoint.yPos;
			double distance = sqrt(xDist * xDist + yDist * yDist);
			if (distance < TANK_THRESHOLD) {
				// Calculate bearing from steering point
				double bearing = atan(xDist / yDist);
				if (yDist < 0) bearing = signum(xDist) * PI + bearing;

				// Calculate bearing relative to steering-point heading
				bearing = Utils.normalRelativeAngle(bearing - steeringPoint.velHeading);

				double thisForce = -sin(bearing) * TANK_REPULSION / (distance * distance);
				enemyForce += thisForce;
			}
		}
		
		// sum up all the forces
		return xWallForce + yWallForce + enemyForce;
	}
	
	private double calculateTargetSteering() {
		double targetForce = 0;
		if (targetCoordinates != null) {
			double xDist = targetCoordinates.xPos - steeringPoint.xPos;
			double yDist = targetCoordinates.yPos - steeringPoint.yPos;
			double distance = sqrt(xDist * xDist + yDist * yDist);
			if (distance > TARGET_THRESHOLD) {
				// Calculate bearing from steering point
				double bearing = atan(xDist / yDist);
				if (yDist < 0) bearing = signum(xDist) * PI + bearing;
	
				// Calculate bearing relative to steering-point heading
				bearing = Utils.normalRelativeAngle(bearing - steeringPoint.velHeading);
	
				targetForce = sin(bearing) * TARGET_ATTRACTION * distance;
			}
		}
		return targetForce;
	}

	/**
	 * We differentiate between steering-force (which helps us avoid obstacles)
	 * and target force (which draws us to a distant target) because we may
	 * limit our velocity in order to turn faster - but this only makes sense
	 * for the steering force.
	 * 
	 * @param steeringForce
	 * @param targetForce
	 */
	private void enterMovementOrders(double steeringForce, double targetForce) {
		// turning has priority - find the maximum speed that supports this
		// turning rate. However, we never want to slow below 2, so that we
		// are always in motion. The maximum allowable speed is 8.
		double speed = 4 * (10 - abs(steeringForce)) / 3;
		speed = min(max(2, speed), 8);
		robot.setMaxVelocity(speed);

		double totalForce = steeringForce + targetForce;
		robot.setTurnRight(totalForce);
		
		if (blockMovement > 0) {
			blockMovement--;
		} else {
			robot.setAhead(30);
		}
	}
}
