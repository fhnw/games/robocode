package ch.fhnw.richards.bradBot;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import robocode.ScannedRobotEvent;

/**
 * Information about the position, velocity and acceleration of an object at a given instant in
 * time. Also present, but not always used, is the "age" of the info - this is only relevant to info
 * obtained by scans of enemies.
 * 
 * This class is used for various purposes: tracking enemies, representing our steering point or
 * aiming point, predicting future target positions, etc. Each of these has its own constructor.
 * 
 * @author Administrator
 * 
 */
public class TrackingInfo {
	private final double STEERING_LEAD = 15.0; // steering point distance from tank
	private ScannedRobotEvent scanEvent;

	public int turn; // which turn does this info represent
	public String name;
	public double xPos;
	public double yPos;
	public double xVel;
	public double yVel;
	public double velocity;
	public double velHeading; // direction of motion, in radians
	public double xAcc;
	public double yAcc;
	public double xAccD; // derivative of xAcc;
	public double yAccD; // derivative of yAcc;
	public int age; // age of info (e.g., from scans)

	/**
	 * Construct tracking info for a steering point, from our own status
	 * 
	 * @param status
	 */
	protected TrackingInfo(Brad.Status status) {
		// velocity vector is the same as ours
		this.velocity = status.velocity;
		this.velHeading = status.headingRadians;

		// set the position of the steering point, relative to velocity
		this.xPos = status.xPos + STEERING_LEAD * sin(this.velHeading) * this.velocity;
		this.yPos = status.yPos + STEERING_LEAD * cos(this.velHeading) * this.velocity;
	}

	/**
	 * Construct tracking-info for an enemy
	 * 
	 * @param enemy
	 */
	protected TrackingInfo(Enemy enemy) {
		scanEvent = enemy.getScanEvent();
		name = enemy.getName();
		age = enemy.getInfoAge();

		xPos = enemy.getX();
		yPos = enemy.getY();

		velocity = scanEvent.getVelocity();
		velHeading = scanEvent.getHeadingRadians();
		xVel = velocity * sin(velHeading);
		yVel = velocity * cos(velHeading);
	}

	/**
	 * create tracking information for the given turn, interpolating between the two events given as
	 * parameters. At the moment, this is simple linear interpolation.
	 * 
	 * @param interpolatedTurn
	 * @param oldTrack
	 * @param newTrack
	 */
	protected TrackingInfo(int interpolatedTurn, TrackingInfo oldTrack, TrackingInfo newTrack) {
		turn = interpolatedTurn;
		double ratio = ((double) (turn - oldTrack.turn)) / ((double) (newTrack.turn - oldTrack.turn));
		xPos = oldTrack.xPos + ratio * (newTrack.xPos - oldTrack.xPos);
		yPos = oldTrack.yPos + ratio * (newTrack.yPos - oldTrack.yPos);
		xVel = oldTrack.xVel + ratio * (newTrack.xVel - oldTrack.xVel);
		yVel = oldTrack.yVel + ratio * (newTrack.yVel - oldTrack.yVel);
		scanEvent = null;
		xAcc = 0;
		yAcc = 0;
	}

	// // Create tracking information from a scan event

	/**
	 * created a predicted location a given number of turns in the future, given the current target
	 * information.
	 * 
	 * @param turns
	 * @param target
	 */
	protected TrackingInfo(Brad robot, double shotSpeed, TrackingInfo pretarget, TrackingInfo target) {
		double totalVel;
		double maxX =robot.getBattleFieldWidth();
		double maxY = robot.getBattleFieldHeight();
		double robotX = robot.getX();
		double robotY = robot.getY();
		scanEvent = null;
		if (pretarget != null) {
			xAccD = target.xAcc - pretarget.xAcc;
			yAccD = target.yAcc - pretarget.yAcc;
		} else {
			xAccD = 0;
			yAccD = 0;
		}
		xAcc = target.xAcc;
		yAcc = target.yAcc;
		xVel = target.xVel;
		yVel = target.yVel;
		xPos = target.xPos;
		yPos = target.yPos;
		int turns = 0; // number of turns ahead we have predicted
		boolean hasHit = false;
		while (!hasHit) {
			xAcc += xAccD;
			if (xAcc < -2) xAcc = -2;
			if (xAcc > 1) xAcc = 1;
			yAcc += yAccD;
			if (yAcc < -2) yAcc = -2;
			if (yAcc > 1) yAcc = 1;
			xVel += xAcc;
			yVel += yAcc;
			totalVel = Math.sqrt(xVel * xVel + yVel * yVel);
			if (totalVel > 8) {
				xVel *= 8 / totalVel;
				yVel *= 8 / totalVel;
			}
			xPos += xVel;
			if (xPos < 0)
				xPos = 0;
			if (xPos > maxX)
			xPos = maxX;
			yPos += yVel;
			if (yPos < 0)
				yPos = 0;
			if (yPos > maxY)
			yPos = maxY;
			
			// has the bullet reached the target yet?
			turns++;
			double distance = Math.sqrt( (robotX - xPos) * (robotX - xPos) + ((robotY - yPos) * (robotY - yPos)) );
			int shotTurns = (int) (distance / shotSpeed);
			hasHit = (turns >= shotTurns);
		}
	}
}