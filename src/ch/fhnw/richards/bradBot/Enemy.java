package ch.fhnw.richards.bradBot;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import robocode.ScannedRobotEvent;
import robocode.util.Utils;

public class Enemy {
	private int infoAge;
	private String name;
	private ScannedRobotEvent scanEvent;	
	private double absoluteBearingRadians;
	private double xPos, yPos;

	// We require our own current info, so that we can calculate the
	// enemy's absolute bearing and position. We must do this now, as we store
	// the scan event, and at a later point in time our info will have
	// changed...
	Enemy(ScannedRobotEvent scanEvent, Brad robot) {
		name = scanEvent.getName(); // for easier access
		setScanEvent(scanEvent, robot);
	}
	public void setScanEvent(ScannedRobotEvent scanEvent, Brad robot) {
		Brad.Status status = robot.getStatus();
		
		this.scanEvent = scanEvent;
		absoluteBearingRadians = Utils.normalRelativeAngle(scanEvent.getBearingRadians() + robot.getHeadingRadians());
		xPos = status.xPos + sin(absoluteBearingRadians) * scanEvent.getDistance();
		yPos = status.yPos + cos(absoluteBearingRadians) * scanEvent.getDistance();
		infoAge = 0;
	}

	public double getX() {
		return xPos;
	}
	public double getY() {
		return yPos;
	}
	public int getInfoAge() {
		return infoAge;
	}
	public void ageInfo() {
		infoAge++;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getAbsoluteBearing() {
		return absoluteBearingRadians * 180/Math.PI;
	}
	public double getAbsoluteBearingRadians() {
		return absoluteBearingRadians;
	}
	public ScannedRobotEvent getScanEvent() {
		return scanEvent;
	}
}
