package ch.fhnw.richards.bradBot;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import robocode.ScannedRobotEvent;
import robocode.util.Utils;

// Record this plus last two scan events (marked with time)
// Three parallel arrays: position, velocity, acceleration.
// Assume constant acceleration (limited by max), predict
// velocity (limited by max) and position (limited by field).
// Finally, calculate firing angle and power.

public class Cannon {
	private Brad robot;
	private Hashtable<String, Enemy> enemies;
	private Radar radar;
	private String lastTarget; // name of our target last turn
	private Enemy target; // the current target
	private ArrayList<TrackingInfo> trackInfo;
	private int turnOffset; // the difference between the turn number and the array position

	protected Cannon(Brad robot) {
		this.robot = robot;
		this.radar = robot.getRadar();
		this.enemies = robot.getEnemies();
		lastTarget = null;
		initialize();
	}

	/**
	 * aimAndFire: called each turn, to let us do our work. We aim and fire completely independently
	 * of the actions of the tank itself and the radar. However, the intent is that we fire at the
	 * target selected by the radar, since it know who is where...
	 */
	public void aimAndFire() {

		target = radar.getTarget();

		// are we still firing at the same target?
		if (!target.getName().equals(lastTarget))
			initialize();

		// update the history for this target
		updateTracking();

		// calculate the aiming point, fire if we can
		aimAndFire2();
	}

	/**
	 * initialize: set up to fire at a new target
	 * 
	 */
	private void initialize() {
		trackInfo = new ArrayList<TrackingInfo>();
		turnOffset = robot.getTurn();
	}

	/**
	 * updateTracking: update our information and predictions about the enemy's movements.
	 * 
	 */
	private void updateTracking() {
		int turnOfLastScan = 0;

		int thisTurn = robot.getTurn();
		TrackingInfo thisTrack = radar.getTargetTrackingInfo();

		// delete all but the last element
		for (int i = 1; i < trackInfo.size(); i++) {
			trackInfo.remove(0);
		}

		if (trackInfo.size() == 0) {
			// first element - nothing else to do...
			trackInfo.add(thisTrack);
		} else {
			if (trackInfo.size() > 0) {
				turnOfLastScan = trackInfo.get(trackInfo.size() - 1).turn;
				// Interpolate any missing elements
				for (int i = turnOfLastScan + 1; i < thisTurn; i++) {
					TrackingInfo lastTrack = trackInfo.get(trackInfo.size() - 1);
					TrackingInfo interp = new TrackingInfo(i, lastTrack, thisTrack);
					trackInfo.add(interp);
				}
			}
			// add this element to the end
			trackInfo.add(thisTrack);

			if (trackInfo.size() > 1) {
				// Update the accelerations for all new elements
				for (int i = turnOfLastScan + 1; i <= thisTurn; i++) {
					int j = i + trackInfo.size() - thisTurn - 1; // difference between turn numbers and array indices
					TrackingInfo tmpTrack1 = trackInfo.get(j - 1);
					TrackingInfo tmpTrack2 = trackInfo.get(j);
					tmpTrack2.xAcc = tmpTrack2.xPos - tmpTrack1.xPos;
					tmpTrack2.yAcc = tmpTrack2.yPos - tmpTrack1.yPos;
				}
			}
		}

	}

	/**
	 * Aim the cannon, using only the enemy's most recent know position.
	 * Note that we aim the cannon even if we are too far
	 * away from the target to fire. This way, the cannon will be pointing at the target if/when we
	 * get closer.
	 */
	private void aimAndFire2() {
		boolean canFire = true; // used to prevent the cannon firing if our aim is unreliable, etc.
		double shotPower; // must be set during aiming, and used when firing
		Brad.Status robotStatus = robot.getStatus();
		TrackingInfo aimingPoint; // where we are aiming the cannon
		TrackingInfo target = trackInfo.get(trackInfo.size() - 1);
		TrackingInfo pretarget = null;
		if (trackInfo.size() > 1) pretarget =trackInfo.get(trackInfo.size() - 2);

		// distance to target's current location
		double distance = Math
				.sqrt((target.xPos - robotStatus.xPos) * (target.xPos - robotStatus.xPos) + (target.yPos - robotStatus.yPos)*(target.yPos - robotStatus.yPos));
		if (distance > 300)
			canFire = false;

		// determine power of our shot
		shotPower = 3;
		if (distance > 150)
			shotPower = 1;
		double shotSpeed = 20 - 3 * shotPower;

		// predict the target's position when the bullet would hit
		// based on the bullet's speed and our current position
		aimingPoint = new TrackingInfo(robot, shotSpeed, pretarget, target);
		
		// distance to aiming point
		distance = Math.sqrt((aimingPoint.xPos - robotStatus.xPos)*(aimingPoint.xPos - robotStatus.xPos)
				+ (aimingPoint.yPos - robotStatus.yPos)*(aimingPoint.yPos - robotStatus.yPos));
		if (distance > 400)
			canFire = false;

		// turn cannon to required heading
		double sineAimBearing = (aimingPoint.xPos - robotStatus.xPos) / distance;
		double aimBearing = Math.asin(sineAimBearing);
		if (robotStatus.yPos > aimingPoint.yPos) {
			if (aimBearing > 0)
				aimBearing = Math.PI - aimBearing;
			else
				aimBearing = -Math.PI - aimBearing;
		}
		double gunTurn = Utils.normalRelativeAngle(aimBearing - robotStatus.gunHeadingRadians);
		robot.setTurnGunRightRadians(gunTurn);

		// fire, if allowed
		if (canFire) {
			robot.setFire(shotPower);
		}
	}
}
