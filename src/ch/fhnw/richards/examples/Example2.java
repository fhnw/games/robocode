package ch.fhnw.richards.examples;

import java.awt.Color;
import robocode.AdvancedRobot;
import robocode.ScannedRobotEvent;

public class Example2 extends AdvancedRobot {
	private Example2Radar radar;
	private Example2Cannon cannon;
	private Example2Tank tank;
	private int turn;
	
	public void run() {
		setColors(Color.BLUE, Color.RED, Color.GREEN);
		setAdjustRadarForGunTurn(true); // our radar, gun and tank
		setAdjustGunForRobotTurn(true); // are independently controlled
		turn = 0;
		
		radar = new Example2Radar(this);
		cannon = new Example2Cannon(this);
		tank = new Example2Tank(this);
		
		while (true) {
			turn++;
			tank.move();
			radar.look();
			cannon.fire();

			// Perform any actions we  planned this turn
			execute();
		}
	}

	/**
	 * onScannedRobot: keep scan info on the last robot seen
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		cannon.enemySeen(turn, e);
	}
	
	// Our components may need information from us - here is the current turn
	public int getTurn() {
		return turn;
		
	}
}
