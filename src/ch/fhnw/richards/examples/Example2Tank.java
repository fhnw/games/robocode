package ch.fhnw.richards.examples;

public class Example2Tank {
	private Example2 robot;

	public Example2Tank(Example2 robot) {
		this.robot = robot;
	}
	
	// We still aren't moving - just turning in a circle
	// This makes us a lovely target for the other tanks!
	protected void move() {
		robot.setTurnRight(10);
		robot.setAhead(50);
	}
}
