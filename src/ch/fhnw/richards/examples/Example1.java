package ch.fhnw.richards.examples;

import robocode.AdvancedRobot;
import robocode.ScannedRobotEvent;

public class Example1 extends AdvancedRobot {
	public void run() {
		// Initialization
		int turn = 0;

		while (true) {
			// One iteration per turn
			turn++;
			System.out.println("Turn " + turn);
			
			// Turn in a circle
			setTurnRight(90);

			// Perform any actions we  planned this turn
			execute();
		}
	}

	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		// Try to fire the gun immediately
		System.out.println("Target ahead - fire!");
		fire(3);
	}

}
