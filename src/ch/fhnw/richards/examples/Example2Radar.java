package ch.fhnw.richards.examples;

public class Example2Radar {
	private Example2 robot;

	public Example2Radar(Example2 robot) {
		this.robot = robot;
	}
	
	// Simplistic: turn the radar as fast as we can
	public void look() {
		robot.setTurnRadarRight(20);
	}
}
