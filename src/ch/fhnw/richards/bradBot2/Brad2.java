package ch.fhnw.richards.bradBot2;

// Angular velocity for predicting opponents position (linear)
import java.awt.Color;
import java.util.Hashtable;

import robocode.AdvancedRobot;
import robocode.HitByBulletEvent;
import robocode.HitWallEvent;
import robocode.RobotDeathEvent;
import robocode.ScannedRobotEvent;
import robocode.SkippedTurnEvent;
import robocode.HitRobotEvent;

/**
 * We have three modes of operation: scan, find and focus.
 * 
 * During the scan phase of 8 turns: We scan all enemies while moving towards
 * the center of the field.
 * 
 * During the find phase, we try to find the enemy who was closest during the
 * scan phase. Once found, we enter the focus phase. If the find phase runs too
 * long, we return to the scan phase.
 * 
 * During the focus phase, we move towards the selected enemy while firing. This
 * phase lasts a limited number of turns, before we return to the scan phase.
 */
public class Brad2 extends AdvancedRobot {
	private Status status;
	private Hashtable<String, Enemy> enemies;
	private int turn;
	private Radar radar;
	private Cannon cannon;
	private Tank tank;
	private Phase phase;
	private int phaseTurns;
	private String target = null;
	private boolean found = false; // target found during the FIND phase?

	public enum Phase {
		SCAN(10), FIND(10), FOCUS(99);

		public final int turns;

		private Phase(int turns) {
			this.turns = turns;
		}
	};

	public void run() {
		setColors(new Color(255, 80, 0), Color.RED, Color.GREEN, Color.GREEN, Color.GREEN);
		setAdjustRadarForGunTurn(true); // our radar, gun and tank
		setAdjustGunForRobotTurn(true); // are independently controlled
		enemies = new Hashtable<String, Enemy>();
		radar = new Radar(this);
		cannon = new Cannon(this);
		tank = new Tank(this);
		turn = 0;
		setPhase(Phase.SCAN);

		while (true) {
			status = new Status();
			turn++;
			phaseTurns--;

			System.out.format("----- %s (%d) -----%n", phase, phaseTurns);
			switch (phase) {
			case SCAN -> {
				target = null;
				tank.center();
				radar.scan();
				if (phaseTurns <= 0) {
					found = false;
					target = findClosestEnemy();
					System.out.println("Target selected: " + target);
					if (target != null) setPhase(Phase.FIND);
				}
			}
			case FIND -> {
				tank.center();
				radar.scan();
				if (found) {
					System.out.println("Target found: " + target);
					setPhase(Phase.FOCUS);
				} else if (phaseTurns <= 0) setPhase(Phase.SCAN);
			}
			case FOCUS -> {
				if (target == null)
					setPhase(Phase.FIND);
				else {
					// Ensure we still have the target
					if ((turn - enemies.get(target).getTurn()) > 2) {
						System.out.println("Target lost");
						setPhase(Phase.SCAN);
					} else {
						System.out.println("Focusing on: " + target);
						tank.chase(target);
						radar.chase(target);
						cannon.fire(target);
						if (phaseTurns <= 0) setPhase(Phase.SCAN);
					}
				}
			}
			}
			execute();
		}
	}

	public void setPhase(Phase phase) {
		this.phase = phase;
		this.phaseTurns = phase.turns;
	}

	private String findClosestEnemy() {
		target = null;
		double distance = Double.MAX_VALUE;
		for (String name : enemies.keySet()) {
			Enemy enemy = enemies.get(name);
			if (enemy.getScanEvent().getDistance() < distance) {
				distance = enemy.getScanEvent().getDistance();
				target = name;
			}
		}
		return target;
	}

	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		if (phase == Phase.SCAN)
			radar.scannedRobot(e, turn);
		else
			found |= radar.scannedRobot(target, e, turn);
	}

	/**
	 * onRobotDeath: What to do when another robot dies
	 */
	public void onRobotDeath(RobotDeathEvent e) {
		enemies.remove(e.getName());
		if (e.getName().equals(target)) setPhase(Phase.SCAN);
	}

	/**
	 * onHitRobot: Collision - pass on to tank, for steering info
	 */
	public void onHitRobot(HitRobotEvent e) {
		tank.hitRobot(e);
	}

	/**
	 * onHitWall: Collision - pass on to tank, for steering info
	 */
	public void onHitWall(HitWallEvent e) {
		tank.hitWall(e);
	}

	/**
	 * onSkippedTurn: Should never happen! This would mean that we took too much
	 * time or called to many methods
	 */
	public void onSkippedTurn(SkippedTurnEvent e) {
		System.out.println("SKIPPED TURN EVENT!");
		turn++;
	}

	/**
	 * onHitByBullet: What to do when you're hit by a bullet
	 */
	public void onHitByBullet(HitByBulletEvent e) {
	}

	public Hashtable<String, Enemy> getEnemies() {
		return enemies;
	}

	public Status getStatus() {
		return status;
	}

	public Radar getRadar() {
		return radar;
	}

	/**
	 * getTurn: get the turn counter
	 * 
	 * @return int turn counter
	 */
	public int getTurn() {
		return turn;
	}

	/**
	 * Information about ourselves, updated each tick
	 * 
	 */
	protected class Status {
		double xPos, yPos;
		double velocity;
		double headingRadians, gunHeadingRadians, radarHeadingRadians;
		double heading, gunHeading, radarHeading;
		double gunHeat;

		private Status() {
			xPos = getX();
			yPos = getY();
			velocity = getVelocity();
			headingRadians = getHeadingRadians();
			gunHeadingRadians = getGunHeadingRadians();
			radarHeadingRadians = getRadarHeadingRadians();
			heading = getHeading();
			gunHeading = getGunHeading();
			radarHeading = getRadarHeading();
			gunHeat = getGunHeat();
		}

		private void print() {
			System.out.println("headings: " + heading + ", " + gunHeading + ", " + radarHeading);
			System.out.println("gun heat: " + gunHeat);
		}
	}

}
