package ch.fhnw.richards.bradBot2;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import robocode.ScannedRobotEvent;
import robocode.util.Utils;

public class Enemy {
	private int turn;
	private ScannedRobotEvent scanEvent;	
	private double headingRadians;
	private double xPos, yPos;

	// We require our own current info, so that we can calculate the
	// enemy's absolute bearing and position. We must do this now, as we store
	// the scan event, and at a later point in time our info will have
	// changed...
	Enemy(ScannedRobotEvent scanEvent, Brad2 robot, int turn) {
		this.turn = turn;

		Brad2.Status status = robot.getStatus();
		this.scanEvent = scanEvent;
		headingRadians = Utils.normalRelativeAngle(scanEvent.getBearingRadians() + robot.getHeadingRadians());
		xPos = status.xPos + sin(headingRadians) * scanEvent.getDistance();
		yPos = status.yPos + cos(headingRadians) * scanEvent.getDistance();
	}

	public int getTurn() {
		return turn;
	}
	public double getX() {
		return xPos;
	}
	public double getY() {
		return yPos;
	}
	public double getAbsoluteBearing() {
		return headingRadians * 180/Math.PI;
	}
	public double getHeadingRadians() {
		return headingRadians;
	}
	public ScannedRobotEvent getScanEvent() {
		return scanEvent;
	}
}
