package ch.fhnw.richards.bradBot2;

import robocode.util.Utils;

public class Cannon {
	private Brad2 robot;

	public Cannon(Brad2 robot) {
		this.robot = robot;
	}

	/**
	 * Firing occurs from the gun's _current_ orientation; turning affects the next turn
	 */
	public void fire(String target) {
		Enemy enemy = robot.getEnemies().get(target);
		double enemyHeading = enemy.getHeadingRadians();
		double gunHeading = Utils.normalRelativeAngle(robot.getStatus().gunHeadingRadians);
		double gunTurn = Utils.normalRelativeAngle(enemyHeading - gunHeading);
		robot.setTurnGunRightRadians(gunTurn * 1.3); // Some extra, in an attempt to anticipate movement
		if (Math.abs(gunTurn) < Math.PI/20 && enemy.getScanEvent().getDistance() < 150)
			robot.setFire(3);
		else if (Math.abs(gunTurn) < Math.PI/10 && enemy.getScanEvent().getDistance() < 300)
			robot.setFire(2);
		else
			robot.setFire(1);
	}

}
