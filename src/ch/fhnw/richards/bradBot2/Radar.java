package ch.fhnw.richards.bradBot2;

import ch.fhnw.richards.bradBot2.Brad2.Phase;
import robocode.ScannedRobotEvent;
import robocode.util.Utils;

public class Radar {
	private Brad2 robot;
	private boolean turnRight = true;
	
	public Radar(Brad2 robot) {
		this.robot = robot;
	}
	
	public void scan() {
		robot.setTurnRadarRightRadians(Math.PI); // Max turn rate
	}
	
	public void chase(String target) {
		Enemy enemy = robot.getEnemies().get(target);
		double enemyHeading = enemy.getHeadingRadians();
		double radarHeading = Utils.normalRelativeAngle(robot.getStatus().radarHeadingRadians);
		double radarTurn = enemyHeading - radarHeading;
		radarTurn += (radarHeading > enemyHeading) ? -Math.PI/10 : Math.PI/10;
		radarTurn = Utils.normalRelativeAngle(radarTurn);
		System.out.format("Radar heading %.2f, enemy heading %.2f, radar turn %.2f%n", radarHeading, enemyHeading, radarTurn);
		robot.setTurnRadarRightRadians(radarTurn);
	}
	
	public void scannedRobot(ScannedRobotEvent e, int turn) {
			String name = e.getName();
			Enemy enemy = new Enemy(e, robot, turn);
			System.out.println("Robot scanned: " + name);
			robot.getEnemies().put(name, enemy);
	}

	public boolean scannedRobot(String target, ScannedRobotEvent e, int turn) {
		scannedRobot(e, turn);
		return (e.getName().equals(target));
	}

}
