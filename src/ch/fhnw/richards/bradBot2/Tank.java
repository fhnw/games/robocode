package ch.fhnw.richards.bradBot2;

import robocode.HitRobotEvent;
import robocode.HitWallEvent;


public class Tank {
	private Brad2 robot;
	private double fieldHeight;
	private double fieldWidth;
	private double centerX;
	private double centerY;
	private boolean centerReached = false;
	
	public Tank(Brad2 robot) {
		this.robot = robot;
		fieldHeight = robot.getBattleFieldHeight();
		fieldWidth = robot.getBattleFieldWidth();
		centerX = fieldWidth / 2;
		centerY = fieldHeight / 2;
	}
	
	/**
	 * Move to approximate center of battlefield, then move in a circle
	 */
	public void center() {
		Brad2.Status status = robot.getStatus();
		double yDiff = centerY-status.yPos;
		double xDiff = centerX-status.xPos;
		double centerDistance = (Math.abs(xDiff) + Math.abs(yDiff)); // Approximate
		if (centerDistance < 100) centerReached = true; // close enough
		else if (centerDistance > 200) centerReached = false; // hysteresis

		if (centerReached) {
			System.out.println("centered");
			robot.setTurnRightRadians(Math.PI / 40); // turn right in large circle	
		} else {
			System.out.format("centering (%.2f)%n", centerDistance);
			double headingToCenter = Math.atan2(xDiff, yDiff);
			double headingDiff = headingToCenter - status.headingRadians;
			while (headingDiff < -Math.PI) headingDiff += 2 * Math.PI;
			while (headingDiff > Math.PI)headingDiff -= 2 * Math.PI; 
			robot.setTurnRightRadians(headingDiff);
		}
		
		robot.setMaxVelocity(4); // 2 to 8
		robot.setAhead(30);
	}
	
	public void chase(String target) {
		Enemy enemy = robot.getEnemies().get(target);
		double enemyBearing = enemy.getScanEvent().getBearingRadians();
		robot.setTurnRightRadians(enemyBearing);
		if (enemy.getScanEvent().getDistance() > 100)
			robot.setMaxVelocity(8);
		else if (enemy.getScanEvent().getDistance() > 50)
			robot.setMaxVelocity(4);
		else
			robot.setMaxVelocity(0);
		robot.setAhead(30);
	}
	
	public void hitRobot(HitRobotEvent e) {
		// TODO
	}
	
	public void hitWall(HitWallEvent e) {
		// TODO
	}
}
